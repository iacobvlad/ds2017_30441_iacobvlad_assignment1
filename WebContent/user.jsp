<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User page</title>
</head>
<body>
<h2>${welcomeMessage}</h2>
<h3>Find flight</h3>
<form action="User" method="get">
	Departs before: <input type="date" name="departDate"><input type="time" name="departTime"><br>
	Arrives before: <input type="date" name="arriveDate"><input type="time" name="arriveTime"><br>
	<input type="submit" name="action" value="Findflight">
	<input type="submit" name="action" value="Refresh">
</form>
<h3>Flights table :</h3>
<table style="background: antiquewhite;    border-spacing: 10px;
    text-align: center;">
		<tr>
			<th>Flight Number</th>
			<th>Airplane Type</th>
			<th>Departure City</th>
			<th>Departure Time</th>
			<th>Arrival City</th>
			<th>Arrival Time</th>
		</tr>
		<c:forEach items="${flights}" var="flight">
			<tr>
				<td>${flight.flightNumber}</td>
				<td>${flight.airplaneType}</td>
				<td>${flight.departureCity}</td>
				<td>${flight.departureDate}</td>
				<td>${flight.arrivalCity}</td>
				<td>${flight.arrivalDate}</td>		
			</tr>
		</c:forEach>
	</table>
</body>
</html>