<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>AdminPage</title>
</head>
<body>
	<h2>Welcome to admin page ${input}</h2>
	<h3>Add a flight :</h3>
	<form action="Admin" method="post">
		Flight Id: <input readonly type="text" name="fId" value=${flight_to_update.flight_id }><br>
		Flight Number : <input type="text" name="fNumber" value=${flight_to_update.flightNumber }><br>
		Airplane Type : <input type="text" name="fAirType" value=${flight_to_update.airplaneType }><br>
		Departure city : <input type="text" name="fDepartureCity" value=${flight_to_update.departureCity }><br>
		Arrival city : <input type="text" name="fArrivalCity" value=${flight_to_update.arrivalCity }><br>
		<input type="submit" value="Add flight">
	</form>
	<h3>Flights table</h3>
	<table style="background: antiquewhite;    border-spacing: 10px;
    text-align: center;">
		<tr>
			<th>Flight Number</th>
			<th>Airplane Type</th>
			<th>Departure City</th>
			<th>Departure Time</th>
			<th>Arrival City</th>
			<th>Arrival Time</th>
			<th>Update</th>
			<th>Delete</th>
		</tr>
		<c:forEach items="${flights}" var="flight">
			<tr>
				<td>${flight.flightNumber}</td>
				<td>${flight.airplaneType}</td>
				<td>${flight.departureCity}</td>
				<td>${flight.departureDate}</td>
				<td>${flight.arrivalCity}</td>
				<td>${flight.arrivalDate}</td>
				<td><a href="/AirportClient/Admin?action=update&id=${flight.flight_id}" style="color: blue; text-decoration: none;">Update</a></td>
				<td><a href="/AirportClient/Admin?action=delete&id=${flight.flight_id}" style="color: red; text-decoration: none;">Delete</a></td>
				
			</tr>
		</c:forEach>
	</table>
</body>
</html>