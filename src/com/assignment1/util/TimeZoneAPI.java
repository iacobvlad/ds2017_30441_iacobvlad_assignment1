package com.assignment1.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

public class TimeZoneAPI {
	
	public Date timeZoneOf(String city) {
		
		String[] coordinates = new String[2];
		try {
			coordinates = getCoordinatesAPI(city);
			if (coordinates != null) {
				Date currentDate = getTimeZoneAPI(coordinates[0], coordinates[1]);
				if (currentDate != null) {
					return currentDate;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new Date();
	}
	
	private String[] getCoordinatesAPI(String city) throws Exception {

		String url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyCN-TR1Fu-hA6Ld5aH7QsvsUXDepR20-ZE";
		String[] coordinatesRes = new String[2];
		try {
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		// optional default is GET
		con.setRequestMethod("GET");
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		JSONObject jsonObject = new JSONObject(response.toString());
		JSONArray array = (JSONArray) jsonObject.get("results");
		JSONObject result = (JSONObject) array.get(0);
		JSONObject geometry = (JSONObject) result.get("geometry");
		JSONObject location = (JSONObject) geometry.get("location");
		Object lat = location.get("lat");
		Object lng = location.get("lng");
		coordinatesRes[0] = lat.toString();
		coordinatesRes[1] = lng.toString();
		}
		catch (Exception e) {
			System.err.println(e);
		}
		return coordinatesRes;
	}
	
	private Date getTimeZoneAPI(String lat, String lng) throws Exception {

		String url = "http://api.timezonedb.com/v2/get-time-zone?key=H42GSHKCLDPM&format=json&by=position&lat="+ lat + "&lng="+ lng;
		String[] coordinatesRes = new String[2];
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		// optional default is GET
		con.setRequestMethod("GET");
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		JSONObject jsonObject = new JSONObject(response.toString());
		String timeStamp = (String) jsonObject.get("formatted");
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 try
	        {
	            Date date = simpleDateFormat.parse(timeStamp);

	            System.out.println("date : "+date);
	            
	            return date;
	        }
	        catch (ParseException ex)
	        {
	            System.out.println("Exception "+ex);
	        }
		return null;
	}
}
