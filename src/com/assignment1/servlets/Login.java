package com.assignment1.servlets;

import java.io.IOException;
import java.sql.Time;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.assignment1.dao.FlightDAO;
import com.assignment1.models.Flight;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private FlightDAO dao;
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
		public Login() {
			super();
		dao = new FlightDAO();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String uname = request.getParameter("uname");
		String pass = request.getParameter("pass");
		if ("admin".equals(uname) && "0".equals(pass)) {
			request.setAttribute("welcomeMessage", "Admin page");
			request.setAttribute("flights", dao.findFlights());
			request.getRequestDispatcher("admin.jsp").forward(request, response);
		}
		else if ("user".equals(uname) && "0".equals(pass)){
			request.setAttribute("welcomeMessage", "Welcome to your Airport Vlad");
			request.setAttribute("flights", dao.findFlights());
			request.getRequestDispatcher("user.jsp").forward(request, response);
		}
		else {
			response.sendRedirect("login.jsp");
		}
	}
	
	
	
	


}

