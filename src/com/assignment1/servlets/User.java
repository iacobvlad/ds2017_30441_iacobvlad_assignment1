package com.assignment1.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.assignment1.dao.FlightDAO;
import com.assignment1.models.Flight;

/**
 * Servlet implementation class User
 */
@WebServlet("/User")
public class User extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FlightDAO dao;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public User() {
		super();
		dao = new FlightDAO();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String action = request.getParameter("action");
		if ("Findflight".equals(action)) {
			String departureTime = request.getParameter("departDate") + " " + request.getParameter("departTime");
			String arrivalTime = request.getParameter("arriveDate") + " " +  request.getParameter("arriveTime");
			request.setAttribute("flights", dao.findFlights(departureTime, arrivalTime));
			request.getRequestDispatcher("user.jsp").forward(request, response);
		}
		else if ("Refresh".equals(action)) {
			request.setAttribute("flights", dao.findFlights());
			request.getRequestDispatcher("user.jsp").forward(request, response);
		}
	  }

	
}
