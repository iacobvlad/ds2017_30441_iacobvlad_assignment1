package com.assignment1.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.assignment1.dao.FlightDAO;
import com.assignment1.models.Flight;
import com.assignment1.util.TimeZoneAPI;

/**
 * Servlet implementation class Admin
 */
@WebServlet("/Admin")
public class Admin extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private FlightDAO dao;
    private TimeZoneAPI timezone;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Admin() {
        super();
        // TODO Auto-generated constructor stub
        dao = new FlightDAO();
        timezone = new TimeZoneAPI();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 * Google Geocoding API KEY : AIzaSyCN-TR1Fu-hA6Ld5aH7QsvsUXDepR20-ZE
	 * TimeZone API KEY: H42GSHKCLDPM
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		if (request.getParameter("action").toString() != null) {
			if (request.getParameter("action").toString().equals("update")) {
				int id = Integer.valueOf(request.getParameter("id"));
				request.setAttribute("flight_to_update", dao.findFlight(id));
				request.setAttribute("flights", dao.findFlights());
				request.getRequestDispatcher("admin.jsp").forward(request, response);
			}
			if (request.getParameter("action").toString().equals("delete")) {
				System.out.println("delete");
				int id = Integer.valueOf(request.getParameter("id"));
				dao.removeFlight(id);
				request.setAttribute("flights", dao.findFlights());
				request.getRequestDispatcher("admin.jsp").forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
			Flight newFlight = new Flight();
			String existing_id = null;
			existing_id = request.getParameter("fId");
			if (existing_id != "") {
				newFlight.setFlight_id(Integer.valueOf(existing_id));
			}
			newFlight.setDepartureDate(timezone.timeZoneOf(request.getParameter("fDepartureCity")));
			newFlight.setArrivalDate(timezone.timeZoneOf(request.getParameter("fArrivalCity")));
			newFlight.setFlightNumber(request.getParameter("fNumber"));
			newFlight.setAirplaneType(request.getParameter("fAirType"));
			newFlight.setDepartureCity((request.getParameter("fDepartureCity")));
			newFlight.setArrivalCity(request.getParameter("fArrivalCity"));
			dao.addFlight(newFlight);
			request.setAttribute("flights", dao.findFlights());
			request.getRequestDispatcher("admin.jsp").forward(request, response);
	}

}
