package com.assignment1.dao;


import java.util.List;



import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.assignment1.models.Flight;




public class FlightDAO {
	
	private SessionFactory factory;
	private Object arrive_date;
	
	public FlightDAO() {
		this.factory = new Configuration().configure().buildSessionFactory();
	}
	
	public void addFlight(Flight f) {
	
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
//			if ("update".equals(operation)) {
//				System.out.println("UPDATE");
//				session.update(f);
//			}
//			else if ("save".equals(operation)) {
//				System.out.println("SAVE");
//				session.save(f);
//			}
			session.saveOrUpdate(f);
			tx.commit();
		}
		catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		}
		finally {
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Flight> findFlights() {
		
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			flights = session.createQuery("FROM Flight").list();
			tx.commit();
			System.out.println("Flights are "+ flights);
		}
		catch(HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			System.err.println(e);
		}
		finally {
			session.close();
		}
		System.out.println("List of "+flights);
		return flights;
	}
	
	@SuppressWarnings("unchecked")
	public Flight findFlight(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE flight_id = :id");
			query.setParameter("id", id);
			flights = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return flights != null && !flights.isEmpty() ? flights.get(0) : null;
	}
	
	public void removeFlight(int id) {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Flight flight = (Flight)session.load(Flight.class, id);
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.delete(flight);
			tx.commit();
		}
		catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
	}

	public List<Flight> findFlights(String depart_date, String arrive_date) {
		// TODO Auto-generated method stub
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			System.out.println(depart_date + arrive_date);
			Query query = session.createQuery("FROM Flight WHERE departure_date >= :depart_date AND arrival_date <= :arrive_date");
			query.setParameter("depart_date", depart_date);
			query.setParameter("arrive_date", arrive_date);
			System.out.println(query.toString());
			flights = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return flights;
	}
}
