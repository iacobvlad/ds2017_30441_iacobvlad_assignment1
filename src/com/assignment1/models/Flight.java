package com.assignment1.models;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


public class Flight {


	private int flight_id;
	
	private String flightNumber;

	private String airplaneType;
	
	private String departureCity;
	
	private String arrivalCity;
	
	private Date departureDate;
	
	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	private Date arrivalDate;
	
	public Flight() {
		
	}

	public Flight(int flight_id, String airplaneType, String departureCity, String arrivalCity, String flightNumber, Date departureDate, Date arrivalDate) {
		
		super();
		this.flight_id = flight_id;
		this.airplaneType = airplaneType;
		this.departureCity = departureCity;
		this.arrivalCity = arrivalCity;
		this.flightNumber = flightNumber;
		this.departureDate = departureDate;
		this.arrivalDate = arrivalDate;
	}
	
	

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public int getFlight_id() {
		return flight_id;
	}

	public void setFlight_id(int flight_id) {
		this.flight_id = flight_id;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getAirplaneType() {
		return airplaneType;
	}

	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}

	public String getDepartureCity() {
		return departureCity;
	}

	public void setDepartureCity(String departureCity) {
		this.departureCity = departureCity;
	}

	public String getArrivalCity() {
		return arrivalCity;
	}

	public void setArrivalCity(String arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

	@Override
	public String toString() {
		return "Flight [flight_id=" + flight_id + ", flightNumber=" + flightNumber + ", airplaneType=" + airplaneType
				+ ", departureCity=" + departureCity + ", arrivalCity=" + arrivalCity + "]";
	}
	

}
