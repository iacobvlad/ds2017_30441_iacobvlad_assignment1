---- Installing Application ---

Before running the application, make sure you have installed the following: 
-JRE 1.8
-MySQL Server
-Tomcat server 8.5
-MySQL Workbench

1. Import the DB schema to your MySQL Workbench
2. Start your Java IDE and import the application as a maven projet
3. Configure the application's server by providing it with the path to your Tomcat installation folder
4. Start the MySQL Server
5. Run the applcation on a TomCat Server
6. After the application has started, acces it on your favorite browers at : localhost:/*your tomcat port(usually 8080*/AirportClient
7. You will be displayed the login page
8. In order to login, use the following credentials

Admin: 

username: Admin
password: 0

User:

username: User
password: 0